// commenting out in JS
//use ctrl + / for one-line
/*
use crtl + shift + / for multi-line
*/
// alert("Hello World"); - this is a statement
// this is also a statement
// console.log allows the browser to display the message that is inside the parenthesis
console.log("Hello World")
console.
log
(
"Hello Again"
)

// Variables
// it is used to store
// any information that is used by an application is stored in what we called "memory";
// when we create variables, certain portions of a device's memory is given "name" that we call "variables"

// if the variable has been called without declaring the variable itself, the console would render that the variable that we are calling is "not defined"

// Declaring a variable
// variables are initialized with the use of let/const keyword
// after declaring the variable (declaring a variable means that we have created a variable and it is ready to receive data), failure to assign a value to it would mean that the variable is "undefined" (the variable is existing but has no value)
	// let myVariable;

let myVariable = "Hello";

console.log(myVariable);

/*
	Guides in writing variables
		-using the right keyword is a way for a dev to seccessfully initialize a variable (let/const)
		-variable names shouldstartwith a lowercase character, and use camelCasing for multiple words
		-variable names should be indicative or descriptive of the value being stored to avoid confusion
*/

// Samples of Variable Guides


/*	let firstName = "Christopher";
	let pokemon = 25000; // a bad namin since we are confused as to what is 25000 gonna do with pokemon

	let FirstName = "Christopher"; // not following the camelCasing
	let firstName = "Christopher";
	
	let first name = "Christopher"; it is not advisable to use spaces in between the words of the variables. we can use underscores


	such examples or camelCasing are:
		lastName emailAddress mobileNumber

	*/

//Declaring and Initializing Variables
let	productName = "Deskstop Computer";
let	productPrice = 18999;
console.log(productName);
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not be changed
// One example in real-world scenario is the interest for loan, savings account, or motgage interest must not be changed due to implications in computation
const interest = 3.539;
console.log(interest);

// Reassigning of variable values
// reassigning a variable calue means that we are going to change the initial or previous value into another value
// we can change the value of a let variable
/*
	SYNTAX:
		letVariableName = newValue;
*/
productName = "Laptop";
console.log(productName);

let	friend = "Toffie";
console.log(friend);

friend = "Pabs";
console.log(friend);

/*
this will return an error since the "friend" variable has already been declared/created

let friend = "Pabs";
console.log(friend);
*/


// const variable values cannot and should not be changed by the devs
/*
interest = 4.489;
console.log(interest)*/
/*
	when to use JS const for a variable?
		as a general rule, always declare a variable with const unless you know that the value will change
*/
// Reassigning vs initializing
let supplier;
// this is technically an initialization since we are assigning a  value to the variable the first time
supplier = "Coca Cola";
console.log(supplier);
// this is reassigning since the value has been reassigned
supplier = "Pepsi";
console.log(supplier);

// var vs let/const

// some of use may wonder why we used let and const in declaring a variable when we search online, we see var

// var - is also used in declaring a variable. but var is an ECMAScript1 (ES1) feature [ES1 (Javascript 1992)]
// let/const - introduced as new features of ES6 (2015)

// difference

// there are issues when it comes to variables declared using var, regarding hoisting.
// in terms of variables, keyword var is hoisted while let/const does not allow hoisting.
// Hoisting is Javascript's default behaviour of moving declaration to the top


a = 5;
console.log(a);
var a;


// scope of variables
/*
	- scope essentially means where thses variables are available for use
	- let and const variables are block scoped
	- a block is a chunk of codes bounded {}. a block lives in curly braces. anything within curly braces is a block
*/


let	outerVariable = "Hello";
{
	let innerVariable = "Hello again";
	console.log(innerVariable);
}

console.log(outerVariable);
/*console.log(innerVariable);*/

// Multiple variable declarations
/*
	multiple variables can be declared in one line using one keyword.
	the two variable declarations must be separated by a comma
	should the second variable not be changed, use separate declarations:
		let	productCode = "CD017";	
		const productBrand = "Dell";
	although removing keywords would use let as default, it is still adviced that we use the correct keyword so that the devs would have a clue as to what type of variable has been created (or is it for reassigning of values)		
*/



let	productCode = "CD017", productBrand = "Dell", productStore = "Makati";
console.log(productCode);
console.log(productBrand);
console.log(productStore);

// trying to use a variable with a reserved keyword
/*const let = "hello";
console.log(let); - since let is already reserved keyword in JS, it is forbidden to use as a name of variables (would return an error)
*/


// Data Types in Javascript
// Strings
// Strings are series of characters that create a word, phrase, a sentence of anything related to creating a text
let country = "Philippines";
console.log(country);


// Cocatenating strings
// we are combining multuple variables with string values with the "+" symbol

let province = "Metro Manila";
console.log(province + "," + country);

// escape character - \ in strings in combination with other characters can produce different effects


// \n - would create a new line break between the text
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress)

// using double quotes and single quotes for string data types are actually vaid in JS.
// if the string has a single quote/apostrophe inside, it is better to use dubles for string indicator so that we wont's have to use the escape character
console.log("John's employees went home early.")
console.log('John\'s employees went home early.')

// Numbers
// integers/whole numbers
// with the exception of strings, other data types in javascript are color coded
let headcount = 26;
console.log(headcount);

// decimal/fractions
let	grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and numbers
// when number and strings are combined, the resulting data type would be a string
console.log("John's grad last quarter is "+grade);




// Boolean
// are normally used to store values relating to the state of certain things
// true/false for the default value
let isMarried = false;
let	inGoodConduct = true;

console.log("isMarried: "+isMarried);
console.log("inGoodConduct: "+inGoodConduct);

// Array
// arrays are special kinds of data type that are used to store multiple values;
// Arrays can store different data tyoes but it is normally used to store similar data types;

/*
	SYNTAX
	let/const varName = [elementA, elementB, elementC, .., elementN]
*/



//similar data type
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades)

// different data type
// it is not advisable to use different data types in an array since it would confusing for other devs when they read our codes
let person = ["John", "Smith", 32, true];
console.log(person)

// object data type
// objects are another special kind of data type that's used to mimic real world objects;
// they are used to create complex data that contains pieces of information that are relevant to each other;





let personDetails = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: true,
	contact: ["09123456789", "09876543210"],
	address:{
		houseNumber: "696",
		city: "Manila"
	}
};
console.log(personDetails);

// typeof keyword - used if the dev are not sure or wants to assure of what the data type of the variable is
console.log(typeof personDetails);



/*
Constant Array/Objects
	the const keyword is a lttle bit misleading when it comes to arrays/objects

	it does not define a constant value for arrays/objects. it defines a constant reference to a value

	we CANNOT:
	Reassign a constant value
	Reassign a constant array
	Reassign a constant object

	but you CAN
	change the elements of a constant array,
	change the properties of a constant object
	


*/

const anime = ["Naruto", "Slam Dunk", "One Piece"];
console.log(anime);

/*
	would return an error because we have const variable
	anime = ["Akame ga Kill"];
*/
anime[0] = ["Akame ga Kill"];
console.log(anime);


//Null data type
let number = 0;
let string = "";
console.log(number);
console.log(string);

let jowa = null;
console.log(jowa);










